"""
=========
trig_functions
=========

trig_functions model template The System Development Kit
Used as a template for all TheSyDeKick Entities.

Current docstring documentation style is Numpy
https://numpydoc.readthedocs.io/en/latest/format.html

This text here is to remind you that documentation is important.
However, you may find it out the even the documentation of this 
entity may be outdated and incomplete. Regardless of that, every day 
and in every way we are getting better and better :).

Initially written by Marko Kosunen, marko.kosunen@aalto.fi, 2017.

"""

import os
import sys
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

from thesdk import *
from rtl import *

import numpy as np
import math

class trig_functions(rtl, thesdk):
    @property
    def _classfile(self):
        return os.path.dirname(os.path.realpath(__file__)) + "/"+__name__

    def __init__(self,*arg): 
        self.print_log(type='I', msg='Inititalizing %s' %(__name__)) 
        self.proplist = [ 'Rs' ];                   # Properties that can be propagated from parent
        self.Rs =  100e6;                           # Sampling frequency
        self.IOS.Members['io_input']=IO()           # Pointer for input data
        self.IOS.Members['io_resultSin']= IO()
        self.IOS.Members['io_resultCos']= IO()

        self.model='py';             # Can be set externally, but is not propagated
        self.par= False              # By default, no parallel processing
        self.queue= []               # By default, no parallel processing

        if len(arg)>=1:
            parent=arg[0]
            self.copy_propval(parent,self.proplist)
            self.parent =parent;

        self.init()

    def init(self):
        pass #Currently nohing to add

    def main(self):
        '''Guideline. Isolate python processing to main method.
        
        To isolate the internal processing from IO connection assigments, 
        The procedure to follow is
        1) Assign input data from input to local variable
        2) Do the processing
        3) Assign local variable to output

        '''
        inval = self.IOS.Members['io_input'].Data
        sinArray = np.array([])
        cosArray = np.array([])
        for input in inval:
            k = math.sqrt(1+2**(-2*0))
            accuracy = 24
            for i in range(1,accuracy - 1):
                k = k*math.sqrt(1+2**(-2*i))

            k = 1/k

            x = []
            y = []
            z = []


            x.append(1)
            y.append(0)

            d = 1

            z.append(input)

            for i in range(accuracy-1):
                if z[i] < 0:
                    d = -1
                else:
                    d = 1
                x.append(x[i] - d*y[i]*2**(-i))
                y.append(y[i] + d*x[i]*2**(-i))
                z.append(z[i] - d*(math.atan(2**(-i))))

            resultSin = y[23]*k
            resultCos = x[23]*k
            sinArray = np.append(sinArray, resultSin)
            cosArray = np.append(cosArray, resultCos)

        out = sinArray

        if self.par:
            self.queue.put(out)
        self.IOS.Members['io_resultSin'].Data = sinArray
        self.IOS.Members['io_resultCos'].Data = cosArray

    def run(self,*arg):
        '''Guideline: Define model depencies of executions in `run` method.

        '''
        if len(arg)>0:
            self.par=True      #flag for parallel processing
            self.queue=arg[0]  #multiprocessing.queue as the first argument
        if self.model=='py':
            self.main()
        else:
            if self.model == 'sv':
                _=rtl_iofile(self, name='io_input', dir='in', iotype='sample', ionames=['io_input'], datatype='sint') # IO file for input input
                _=rtl_iofile(self, name='io_resultSin', dir='out', iotype='sample', ionames=['io_resultSin'], datatype='sint') # IO file for output resultSin
                _=rtl_iofile(self, name='io_resultCos', dir='out', iotype='sample', ionames=['io_resultCos'], datatype='sint') # IO file for output resultCos
                self.rtlparameters=dict([ ('g_Rs',self.Rs),]) # Defines the sample rate
                self.run_rtl()
                self.IOS.Members['io_resultSin'].Data=self.IOS.Members['io_resultSin'].Data.astype(int).reshape(-1,1)
                self.IOS.Members['io_resultCos'].Data=self.IOS.Members['io_resultCos'].Data.astype(int).reshape(-1,1)
    def define_io_conditions(self):
        '''This overloads the method called by run_rtl method. It defines the read/write conditions for the files

        '''
        # Input A is read to verilog simulation after 'initdone' is set to 1 by controller
        self.iofile_bundle.Members['io_input'].verilog_io_condition='initdone'
        # Output is read to verilog simulation when all of the outputs are valid, 
        # and after 'initdone' is set to 1 by controller
        self.iofile_bundle.Members['io_resultSin'].verilog_io_condition_append(cond='&& initdone')
        self.iofile_bundle.Members['io_resultCos'].verilog_io_condition_append(cond='&& initdone')


if __name__=="__main__":
    import matplotlib.pyplot as plt
    from  trig_functions import *
    from  trig_functions.controller import controller as trig_functions_controller
    import pdb
    import math
    length=1024
    rs=100e6
    indata = np.array([i * 0.001 for i in range(0, 1570)]).reshape(-1,1)
    referenceDataSin = np.array([np.sin(i * 0.001) for i in range(0, 1570)])
    referenceDataCos = np.array([np.cos(i * 0.001) for i in range(0, 1570)])
    # Different reference values for verilog
    referenceDataSinSV = np.array([(int(np.sin(i * 0.001) * 100000000000) << 29)/100000000000 for i in range(0, 1570)])
    referenceDataCosSV = np.array([(int(np.cos(i * 0.001) * 100000000000) << 29)/100000000000 for i in range(0, 1570)])

    controller=trig_functions_controller()
    controller.Rs=rs
    controller.reset()
    controller.start_datafeed()



    models=[ 'py', 'sv']
    duts=[]
    for model in models:
        d=trig_functions()
        duts.append(d) 
        d.model=model
        d.Rs=rs
        d.preserve_iofiles = False
        if model == 'sv':
            # For verilog model we want to bit shift the input for 29 bits
            d.IOS.Members['io_input'].Data = np.array([int((i << 29) * 0.001) for i in range(0, 1570)]).reshape(-1,1)
        else:
            d.IOS.Members['io_input'].Data = indata

        d.IOS.Members['control_write']=controller.IOS.Members['control_write']
        d.init()
        d.run()


    ##Python plots 

    plt.figure()
    plt.title('Python model sin(x) absolute error: abs(reference-python)',fontsize=20)
    x = np.arange(len(indata)).reshape(-1,1)
    absolute_error = np.abs(np.subtract(duts[0].IOS.Members['io_resultSin'].Data, referenceDataSin))
    plt.xlim([np.amin(x), np.amax(x)])
    plt.ylim([0, max(absolute_error)])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Absolute error sin(x)", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, absolute_error)

    plt.figure()
    plt.title('Python model cos(x) absolute error: abs(reference-python)',fontsize=20)
    x = np.arange(len(indata)).reshape(-1,1)
    absolute_error = np.abs(np.subtract(duts[0].IOS.Members['io_resultCos'].Data, referenceDataCos))
    plt.xlim([np.amin(x), np.amax(x)])
    plt.ylim([0, max(absolute_error)])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Absolute error cos(x)", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, absolute_error)

    plt.figure()
    plt.title('Python model sin(x) relative error: (reference/python)',fontsize=20)
    x = np.arange(len(indata)).reshape(-1,1)
    output = duts[0].IOS.Members['io_resultSin'].Data
    relative_error = np.divide(output, referenceDataSin, out=np.zeros_like(output), where=referenceDataSin!=0)
    plt.xlim([np.amin(x), np.amax(x)])
    plt.ylim([0.999999,1.000001])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Relative error sin(x)", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, relative_error)

    plt.figure()
    plt.title('Python model cos(x) relative error: (reference/python)',fontsize=20)
    x = np.arange(len(indata)).reshape(-1,1)
    output = duts[0].IOS.Members['io_resultCos'].Data
    relative_error = np.divide(output, referenceDataCos, out=np.zeros_like(output), where=referenceDataCos!=0)
    plt.xlim([np.amin(x), np.amax(x)])
    plt.ylim([0.999999,1.000001])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Relative error cos(x)", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, relative_error)
    
    
    
    

    ##Verilog plots

    plt.title('Python model sin(x) absolute error: abs(reference-python)',fontsize=40)
    x = np.arange(len(indata)).reshape(-1,1)
    absolute_error = np.abs(np.subtract(duts[1].IOS.Members['io_resultSin'].Data, referenceDataSinSV))
    plt.xlim([np.amin(x), np.amax(x)])
    plt.ylim([0, max(absolute_error)])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Absolute error sin(x)", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, absolute_error)

    plt.figure()
    plt.title('Python model cos(x) absolute error: abs(reference-python)',fontsize=40)
    x = np.arange(len(indata)).reshape(-1,1)
    absolute_error = np.abs(np.subtract(duts[1].IOS.Members['io_resultCos'].Data, referenceDataCosSV))
    plt.xlim([np.amin(x), np.amax(x)])
    plt.ylim([0, max(absolute_error)])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Absolute error cos(x)", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, absolute_error)

    plt.figure()
    plt.title('Python model sin(x) relative error: (reference/python)',fontsize=40)
    x = np.arange(len(indata)).reshape(-1,1)
    output = duts[1].IOS.Members['io_resultSin'].Data
    relative_error = np.divide(output, referenceDataSinSV, out=np.zeros_like(output), where=referenceDataSinSV!=0)
    plt.xlim([np.amin(x), np.amax(x)])
    plt.ylim([min(relative_error), max(relative_error)])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Relative error sin(x)", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, relative_error)

    plt.figure()
    plt.title('Python model cos(x) relative error: (reference/python)',fontsize=40)
    x = np.arange(len(indata)).reshape(-1,1)
    output = duts[1].IOS.Members['io_resultCos'].Data
    relative_error = np.divide(output, referenceDataCosSV, out=np.zeros_like(output), where=referenceDataCosSV!=0)
    plt.xlim([np.amin(x), np.amax(x)])
    plt.ylim([min(relative_error), max(relative_error)])
    plt.xlabel("Input", fontsize = 24)
    plt.xticks(fontsize = 18)
    plt.ylabel("Relative error cos(x)", fontsize = 24)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.plot(x, relative_error)


    plt.show()
    input()        
                                                        
