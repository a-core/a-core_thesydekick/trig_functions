add wave -position insertpoint  \
sim/:tb_trig_functions:io_input \
sim/:tb_trig_functions:initdone \
sim/:tb_trig_functions:io_resultSin \
sim/:tb_trig_functions:io_resultCos \
run -all
wave zoom full
