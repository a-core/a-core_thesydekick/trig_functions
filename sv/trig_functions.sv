module trig_functions(
  input         clock,
  input         reset,
  input  [31:0] io_input,
  output [31:0] io_resultSin,
  output [31:0] io_resultCos
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
  reg [31:0] _RAND_5;
  reg [31:0] _RAND_6;
  reg [31:0] _RAND_7;
  reg [31:0] _RAND_8;
  reg [31:0] _RAND_9;
  reg [31:0] _RAND_10;
  reg [31:0] _RAND_11;
  reg [31:0] _RAND_12;
  reg [31:0] _RAND_13;
  reg [31:0] _RAND_14;
  reg [31:0] _RAND_15;
  reg [31:0] _RAND_16;
  reg [31:0] _RAND_17;
  reg [31:0] _RAND_18;
  reg [31:0] _RAND_19;
  reg [31:0] _RAND_20;
  reg [31:0] _RAND_21;
  reg [31:0] _RAND_22;
  reg [31:0] _RAND_23;
  reg [31:0] _RAND_24;
  reg [31:0] _RAND_25;
  reg [31:0] _RAND_26;
  reg [31:0] _RAND_27;
  reg [31:0] _RAND_28;
  reg [31:0] _RAND_29;
  reg [31:0] _RAND_30;
  reg [31:0] _RAND_31;
  reg [31:0] _RAND_32;
  reg [31:0] _RAND_33;
  reg [31:0] _RAND_34;
  reg [31:0] _RAND_35;
  reg [31:0] _RAND_36;
  reg [31:0] _RAND_37;
  reg [31:0] _RAND_38;
  reg [31:0] _RAND_39;
  reg [31:0] _RAND_40;
  reg [31:0] _RAND_41;
  reg [31:0] _RAND_42;
  reg [31:0] _RAND_43;
  reg [31:0] _RAND_44;
  reg [31:0] _RAND_45;
  reg [31:0] _RAND_46;
  reg [31:0] _RAND_47;
  reg [31:0] _RAND_48;
  reg [31:0] _RAND_49;
  reg [31:0] _RAND_50;
  reg [31:0] _RAND_51;
  reg [31:0] _RAND_52;
  reg [31:0] _RAND_53;
  reg [31:0] _RAND_54;
  reg [31:0] _RAND_55;
  reg [31:0] _RAND_56;
  reg [31:0] _RAND_57;
  reg [31:0] _RAND_58;
  reg [31:0] _RAND_59;
  reg [31:0] _RAND_60;
  reg [31:0] _RAND_61;
  reg [31:0] _RAND_62;
  reg [31:0] _RAND_63;
  reg [31:0] _RAND_64;
  reg [31:0] _RAND_65;
  reg [31:0] _RAND_66;
  reg [31:0] _RAND_67;
  reg [31:0] _RAND_68;
  reg [31:0] _RAND_69;
  reg [31:0] _RAND_70;
  reg [31:0] _RAND_71;
  reg [31:0] _RAND_72;
  reg [31:0] _RAND_73;
  reg [31:0] _RAND_74;
  reg [31:0] _RAND_75;
  reg [31:0] _RAND_76;
  reg [31:0] _RAND_77;
  reg [31:0] _RAND_78;
  reg [31:0] _RAND_79;
  reg [31:0] _RAND_80;
  reg [31:0] _RAND_81;
  reg [31:0] _RAND_82;
  reg [31:0] _RAND_83;
  reg [31:0] _RAND_84;
  reg [31:0] _RAND_85;
  reg [31:0] _RAND_86;
  reg [31:0] _RAND_87;
  reg [31:0] _RAND_88;
  reg [31:0] _RAND_89;
  reg [31:0] _RAND_90;
  reg [31:0] _RAND_91;
  reg [31:0] _RAND_92;
`endif // RANDOMIZE_REG_INIT
  reg [31:0] x_0; // @[Trig3.scala 15:18]
  reg [31:0] x_1; // @[Trig3.scala 15:18]
  reg [31:0] x_2; // @[Trig3.scala 15:18]
  reg [31:0] x_3; // @[Trig3.scala 15:18]
  reg [31:0] x_4; // @[Trig3.scala 15:18]
  reg [31:0] x_5; // @[Trig3.scala 15:18]
  reg [31:0] x_6; // @[Trig3.scala 15:18]
  reg [31:0] x_7; // @[Trig3.scala 15:18]
  reg [31:0] x_8; // @[Trig3.scala 15:18]
  reg [31:0] x_9; // @[Trig3.scala 15:18]
  reg [31:0] x_10; // @[Trig3.scala 15:18]
  reg [31:0] x_11; // @[Trig3.scala 15:18]
  reg [31:0] x_12; // @[Trig3.scala 15:18]
  reg [31:0] x_13; // @[Trig3.scala 15:18]
  reg [31:0] x_14; // @[Trig3.scala 15:18]
  reg [31:0] x_15; // @[Trig3.scala 15:18]
  reg [31:0] x_16; // @[Trig3.scala 15:18]
  reg [31:0] x_17; // @[Trig3.scala 15:18]
  reg [31:0] x_18; // @[Trig3.scala 15:18]
  reg [31:0] x_19; // @[Trig3.scala 15:18]
  reg [31:0] x_20; // @[Trig3.scala 15:18]
  reg [31:0] x_21; // @[Trig3.scala 15:18]
  reg [31:0] x_22; // @[Trig3.scala 15:18]
  reg [31:0] x_23; // @[Trig3.scala 15:18]
  reg [31:0] y_1; // @[Trig3.scala 16:18]
  reg [31:0] y_2; // @[Trig3.scala 16:18]
  reg [31:0] y_3; // @[Trig3.scala 16:18]
  reg [31:0] y_4; // @[Trig3.scala 16:18]
  reg [31:0] y_5; // @[Trig3.scala 16:18]
  reg [31:0] y_6; // @[Trig3.scala 16:18]
  reg [31:0] y_7; // @[Trig3.scala 16:18]
  reg [31:0] y_8; // @[Trig3.scala 16:18]
  reg [31:0] y_9; // @[Trig3.scala 16:18]
  reg [31:0] y_10; // @[Trig3.scala 16:18]
  reg [31:0] y_11; // @[Trig3.scala 16:18]
  reg [31:0] y_12; // @[Trig3.scala 16:18]
  reg [31:0] y_13; // @[Trig3.scala 16:18]
  reg [31:0] y_14; // @[Trig3.scala 16:18]
  reg [31:0] y_15; // @[Trig3.scala 16:18]
  reg [31:0] y_16; // @[Trig3.scala 16:18]
  reg [31:0] y_17; // @[Trig3.scala 16:18]
  reg [31:0] y_18; // @[Trig3.scala 16:18]
  reg [31:0] y_19; // @[Trig3.scala 16:18]
  reg [31:0] y_20; // @[Trig3.scala 16:18]
  reg [31:0] y_21; // @[Trig3.scala 16:18]
  reg [31:0] y_22; // @[Trig3.scala 16:18]
  reg [31:0] y_23; // @[Trig3.scala 16:18]
  reg [31:0] z_0; // @[Trig3.scala 17:18]
  reg [31:0] z_1; // @[Trig3.scala 17:18]
  reg [31:0] z_2; // @[Trig3.scala 17:18]
  reg [31:0] z_3; // @[Trig3.scala 17:18]
  reg [31:0] z_4; // @[Trig3.scala 17:18]
  reg [31:0] z_5; // @[Trig3.scala 17:18]
  reg [31:0] z_6; // @[Trig3.scala 17:18]
  reg [31:0] z_7; // @[Trig3.scala 17:18]
  reg [31:0] z_8; // @[Trig3.scala 17:18]
  reg [31:0] z_9; // @[Trig3.scala 17:18]
  reg [31:0] z_10; // @[Trig3.scala 17:18]
  reg [31:0] z_11; // @[Trig3.scala 17:18]
  reg [31:0] z_12; // @[Trig3.scala 17:18]
  reg [31:0] z_13; // @[Trig3.scala 17:18]
  reg [31:0] z_14; // @[Trig3.scala 17:18]
  reg [31:0] z_15; // @[Trig3.scala 17:18]
  reg [31:0] z_16; // @[Trig3.scala 17:18]
  reg [31:0] z_17; // @[Trig3.scala 17:18]
  reg [31:0] z_18; // @[Trig3.scala 17:18]
  reg [31:0] z_19; // @[Trig3.scala 17:18]
  reg [31:0] z_20; // @[Trig3.scala 17:18]
  reg [31:0] z_21; // @[Trig3.scala 17:18]
  reg [31:0] z_22; // @[Trig3.scala 17:18]
  reg [31:0] d_1; // @[Trig3.scala 18:18]
  reg [31:0] d_2; // @[Trig3.scala 18:18]
  reg [31:0] d_3; // @[Trig3.scala 18:18]
  reg [31:0] d_4; // @[Trig3.scala 18:18]
  reg [31:0] d_5; // @[Trig3.scala 18:18]
  reg [31:0] d_6; // @[Trig3.scala 18:18]
  reg [31:0] d_7; // @[Trig3.scala 18:18]
  reg [31:0] d_8; // @[Trig3.scala 18:18]
  reg [31:0] d_9; // @[Trig3.scala 18:18]
  reg [31:0] d_10; // @[Trig3.scala 18:18]
  reg [31:0] d_11; // @[Trig3.scala 18:18]
  reg [31:0] d_12; // @[Trig3.scala 18:18]
  reg [31:0] d_13; // @[Trig3.scala 18:18]
  reg [31:0] d_14; // @[Trig3.scala 18:18]
  reg [31:0] d_15; // @[Trig3.scala 18:18]
  reg [31:0] d_16; // @[Trig3.scala 18:18]
  reg [31:0] d_17; // @[Trig3.scala 18:18]
  reg [31:0] d_18; // @[Trig3.scala 18:18]
  reg [31:0] d_19; // @[Trig3.scala 18:18]
  reg [31:0] d_20; // @[Trig3.scala 18:18]
  reg [31:0] d_21; // @[Trig3.scala 18:18]
  reg [31:0] d_22; // @[Trig3.scala 18:18]
  reg [31:0] d_23; // @[Trig3.scala 18:18]
  wire [63:0] _x_1_T = $signed(d_1) * 32'sh0; // @[Trig3.scala 42:29]
  wire [63:0] _GEN_0 = {{32{x_0[31]}},x_0}; // @[Trig3.scala 42:22]
  wire [63:0] _x_1_T_4 = $signed(_GEN_0) - $signed(_x_1_T); // @[Trig3.scala 42:22]
  wire [63:0] _y_1_T = $signed(d_1) * $signed(x_0); // @[Trig3.scala 43:29]
  wire [64:0] _y_1_T_2 = {{1{_y_1_T[63]}},_y_1_T}; // @[Trig3.scala 43:22]
  wire [63:0] _y_1_T_4 = _y_1_T_2[63:0]; // @[Trig3.scala 43:22]
  wire [63:0] _z_1_T = $signed(d_1) * 32'sh1921fb54; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_1 = {{32{z_0[31]}},z_0}; // @[Trig3.scala 44:22]
  wire [63:0] _z_1_T_3 = $signed(_GEN_1) - $signed(_z_1_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_2_T = $signed(d_2) * $signed(y_1); // @[Trig3.scala 42:29]
  wire [62:0] _x_2_T_1 = _x_2_T[63:1]; // @[Trig3.scala 42:37]
  wire [62:0] _GEN_2 = {{31{x_1[31]}},x_1}; // @[Trig3.scala 42:22]
  wire [62:0] _x_2_T_4 = $signed(_GEN_2) - $signed(_x_2_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_2_T = $signed(d_2) * $signed(x_1); // @[Trig3.scala 43:29]
  wire [62:0] _y_2_T_1 = _y_2_T[63:1]; // @[Trig3.scala 43:37]
  wire [62:0] _GEN_3 = {{31{y_1[31]}},y_1}; // @[Trig3.scala 43:22]
  wire [62:0] _y_2_T_4 = $signed(_GEN_3) + $signed(_y_2_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_2_T = $signed(d_2) * 32'shed63382; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_4 = {{32{z_1[31]}},z_1}; // @[Trig3.scala 44:22]
  wire [63:0] _z_2_T_3 = $signed(_GEN_4) - $signed(_z_2_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_3_T = $signed(d_3) * $signed(y_2); // @[Trig3.scala 42:29]
  wire [61:0] _x_3_T_1 = _x_3_T[63:2]; // @[Trig3.scala 42:37]
  wire [61:0] _GEN_5 = {{30{x_2[31]}},x_2}; // @[Trig3.scala 42:22]
  wire [61:0] _x_3_T_4 = $signed(_GEN_5) - $signed(_x_3_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_3_T = $signed(d_3) * $signed(x_2); // @[Trig3.scala 43:29]
  wire [61:0] _y_3_T_1 = _y_3_T[63:2]; // @[Trig3.scala 43:37]
  wire [61:0] _GEN_6 = {{30{y_2[31]}},y_2}; // @[Trig3.scala 43:22]
  wire [61:0] _y_3_T_4 = $signed(_GEN_6) + $signed(_y_3_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_3_T = $signed(d_3) * 32'sh7d6dd7e; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_7 = {{32{z_2[31]}},z_2}; // @[Trig3.scala 44:22]
  wire [63:0] _z_3_T_3 = $signed(_GEN_7) - $signed(_z_3_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_4_T = $signed(d_4) * $signed(y_3); // @[Trig3.scala 42:29]
  wire [60:0] _x_4_T_1 = _x_4_T[63:3]; // @[Trig3.scala 42:37]
  wire [60:0] _GEN_8 = {{29{x_3[31]}},x_3}; // @[Trig3.scala 42:22]
  wire [60:0] _x_4_T_4 = $signed(_GEN_8) - $signed(_x_4_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_4_T = $signed(d_4) * $signed(x_3); // @[Trig3.scala 43:29]
  wire [60:0] _y_4_T_1 = _y_4_T[63:3]; // @[Trig3.scala 43:37]
  wire [60:0] _GEN_9 = {{29{y_3[31]}},y_3}; // @[Trig3.scala 43:22]
  wire [60:0] _y_4_T_4 = $signed(_GEN_9) + $signed(_y_4_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_4_T = $signed(d_4) * 32'sh3fab753; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_10 = {{32{z_3[31]}},z_3}; // @[Trig3.scala 44:22]
  wire [63:0] _z_4_T_3 = $signed(_GEN_10) - $signed(_z_4_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_5_T = $signed(d_5) * $signed(y_4); // @[Trig3.scala 42:29]
  wire [59:0] _x_5_T_1 = _x_5_T[63:4]; // @[Trig3.scala 42:37]
  wire [59:0] _GEN_11 = {{28{x_4[31]}},x_4}; // @[Trig3.scala 42:22]
  wire [59:0] _x_5_T_4 = $signed(_GEN_11) - $signed(_x_5_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_5_T = $signed(d_5) * $signed(x_4); // @[Trig3.scala 43:29]
  wire [59:0] _y_5_T_1 = _y_5_T[63:4]; // @[Trig3.scala 43:37]
  wire [59:0] _GEN_12 = {{28{y_4[31]}},y_4}; // @[Trig3.scala 43:22]
  wire [59:0] _y_5_T_4 = $signed(_GEN_12) + $signed(_y_5_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_5_T = $signed(d_5) * 32'sh1ff55bb; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_13 = {{32{z_4[31]}},z_4}; // @[Trig3.scala 44:22]
  wire [63:0] _z_5_T_3 = $signed(_GEN_13) - $signed(_z_5_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_6_T = $signed(d_6) * $signed(y_5); // @[Trig3.scala 42:29]
  wire [58:0] _x_6_T_1 = _x_6_T[63:5]; // @[Trig3.scala 42:37]
  wire [58:0] _GEN_14 = {{27{x_5[31]}},x_5}; // @[Trig3.scala 42:22]
  wire [58:0] _x_6_T_4 = $signed(_GEN_14) - $signed(_x_6_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_6_T = $signed(d_6) * $signed(x_5); // @[Trig3.scala 43:29]
  wire [58:0] _y_6_T_1 = _y_6_T[63:5]; // @[Trig3.scala 43:37]
  wire [58:0] _GEN_15 = {{27{y_5[31]}},y_5}; // @[Trig3.scala 43:22]
  wire [58:0] _y_6_T_4 = $signed(_GEN_15) + $signed(_y_6_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_6_T = $signed(d_6) * 32'shffeaad; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_16 = {{32{z_5[31]}},z_5}; // @[Trig3.scala 44:22]
  wire [63:0] _z_6_T_3 = $signed(_GEN_16) - $signed(_z_6_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_7_T = $signed(d_7) * $signed(y_6); // @[Trig3.scala 42:29]
  wire [57:0] _x_7_T_1 = _x_7_T[63:6]; // @[Trig3.scala 42:37]
  wire [57:0] _GEN_17 = {{26{x_6[31]}},x_6}; // @[Trig3.scala 42:22]
  wire [57:0] _x_7_T_4 = $signed(_GEN_17) - $signed(_x_7_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_7_T = $signed(d_7) * $signed(x_6); // @[Trig3.scala 43:29]
  wire [57:0] _y_7_T_1 = _y_7_T[63:6]; // @[Trig3.scala 43:37]
  wire [57:0] _GEN_18 = {{26{y_6[31]}},y_6}; // @[Trig3.scala 43:22]
  wire [57:0] _y_7_T_4 = $signed(_GEN_18) + $signed(_y_7_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_7_T = $signed(d_7) * 32'sh7ffd55; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_19 = {{32{z_6[31]}},z_6}; // @[Trig3.scala 44:22]
  wire [63:0] _z_7_T_3 = $signed(_GEN_19) - $signed(_z_7_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_8_T = $signed(d_8) * $signed(y_7); // @[Trig3.scala 42:29]
  wire [56:0] _x_8_T_1 = _x_8_T[63:7]; // @[Trig3.scala 42:37]
  wire [56:0] _GEN_20 = {{25{x_7[31]}},x_7}; // @[Trig3.scala 42:22]
  wire [56:0] _x_8_T_4 = $signed(_GEN_20) - $signed(_x_8_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_8_T = $signed(d_8) * $signed(x_7); // @[Trig3.scala 43:29]
  wire [56:0] _y_8_T_1 = _y_8_T[63:7]; // @[Trig3.scala 43:37]
  wire [56:0] _GEN_21 = {{25{y_7[31]}},y_7}; // @[Trig3.scala 43:22]
  wire [56:0] _y_8_T_4 = $signed(_GEN_21) + $signed(_y_8_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_8_T = $signed(d_8) * 32'sh3fffaa; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_22 = {{32{z_7[31]}},z_7}; // @[Trig3.scala 44:22]
  wire [63:0] _z_8_T_3 = $signed(_GEN_22) - $signed(_z_8_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_9_T = $signed(d_9) * $signed(y_8); // @[Trig3.scala 42:29]
  wire [55:0] _x_9_T_1 = _x_9_T[63:8]; // @[Trig3.scala 42:37]
  wire [55:0] _GEN_23 = {{24{x_8[31]}},x_8}; // @[Trig3.scala 42:22]
  wire [55:0] _x_9_T_4 = $signed(_GEN_23) - $signed(_x_9_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_9_T = $signed(d_9) * $signed(x_8); // @[Trig3.scala 43:29]
  wire [55:0] _y_9_T_1 = _y_9_T[63:8]; // @[Trig3.scala 43:37]
  wire [55:0] _GEN_24 = {{24{y_8[31]}},y_8}; // @[Trig3.scala 43:22]
  wire [55:0] _y_9_T_4 = $signed(_GEN_24) + $signed(_y_9_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_9_T = $signed(d_9) * 32'sh1ffff5; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_25 = {{32{z_8[31]}},z_8}; // @[Trig3.scala 44:22]
  wire [63:0] _z_9_T_3 = $signed(_GEN_25) - $signed(_z_9_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_10_T = $signed(d_10) * $signed(y_9); // @[Trig3.scala 42:29]
  wire [54:0] _x_10_T_1 = _x_10_T[63:9]; // @[Trig3.scala 42:37]
  wire [54:0] _GEN_26 = {{23{x_9[31]}},x_9}; // @[Trig3.scala 42:22]
  wire [54:0] _x_10_T_4 = $signed(_GEN_26) - $signed(_x_10_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_10_T = $signed(d_10) * $signed(x_9); // @[Trig3.scala 43:29]
  wire [54:0] _y_10_T_1 = _y_10_T[63:9]; // @[Trig3.scala 43:37]
  wire [54:0] _GEN_27 = {{23{y_9[31]}},y_9}; // @[Trig3.scala 43:22]
  wire [54:0] _y_10_T_4 = $signed(_GEN_27) + $signed(_y_10_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_10_T = $signed(d_10) * 32'shffffe; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_28 = {{32{z_9[31]}},z_9}; // @[Trig3.scala 44:22]
  wire [63:0] _z_10_T_3 = $signed(_GEN_28) - $signed(_z_10_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_11_T = $signed(d_11) * $signed(y_10); // @[Trig3.scala 42:29]
  wire [53:0] _x_11_T_1 = _x_11_T[63:10]; // @[Trig3.scala 42:37]
  wire [53:0] _GEN_29 = {{22{x_10[31]}},x_10}; // @[Trig3.scala 42:22]
  wire [53:0] _x_11_T_4 = $signed(_GEN_29) - $signed(_x_11_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_11_T = $signed(d_11) * $signed(x_10); // @[Trig3.scala 43:29]
  wire [53:0] _y_11_T_1 = _y_11_T[63:10]; // @[Trig3.scala 43:37]
  wire [53:0] _GEN_30 = {{22{y_10[31]}},y_10}; // @[Trig3.scala 43:22]
  wire [53:0] _y_11_T_4 = $signed(_GEN_30) + $signed(_y_11_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_11_T = $signed(d_11) * 32'sh7ffff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_31 = {{32{z_10[31]}},z_10}; // @[Trig3.scala 44:22]
  wire [63:0] _z_11_T_3 = $signed(_GEN_31) - $signed(_z_11_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_12_T = $signed(d_12) * $signed(y_11); // @[Trig3.scala 42:29]
  wire [52:0] _x_12_T_1 = _x_12_T[63:11]; // @[Trig3.scala 42:37]
  wire [52:0] _GEN_32 = {{21{x_11[31]}},x_11}; // @[Trig3.scala 42:22]
  wire [52:0] _x_12_T_4 = $signed(_GEN_32) - $signed(_x_12_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_12_T = $signed(d_12) * $signed(x_11); // @[Trig3.scala 43:29]
  wire [52:0] _y_12_T_1 = _y_12_T[63:11]; // @[Trig3.scala 43:37]
  wire [52:0] _GEN_33 = {{21{y_11[31]}},y_11}; // @[Trig3.scala 43:22]
  wire [52:0] _y_12_T_4 = $signed(_GEN_33) + $signed(_y_12_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_12_T = $signed(d_12) * 32'sh3ffff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_34 = {{32{z_11[31]}},z_11}; // @[Trig3.scala 44:22]
  wire [63:0] _z_12_T_3 = $signed(_GEN_34) - $signed(_z_12_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_13_T = $signed(d_13) * $signed(y_12); // @[Trig3.scala 42:29]
  wire [51:0] _x_13_T_1 = _x_13_T[63:12]; // @[Trig3.scala 42:37]
  wire [51:0] _GEN_35 = {{20{x_12[31]}},x_12}; // @[Trig3.scala 42:22]
  wire [51:0] _x_13_T_4 = $signed(_GEN_35) - $signed(_x_13_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_13_T = $signed(d_13) * $signed(x_12); // @[Trig3.scala 43:29]
  wire [51:0] _y_13_T_1 = _y_13_T[63:12]; // @[Trig3.scala 43:37]
  wire [51:0] _GEN_36 = {{20{y_12[31]}},y_12}; // @[Trig3.scala 43:22]
  wire [51:0] _y_13_T_4 = $signed(_GEN_36) + $signed(_y_13_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_13_T = $signed(d_13) * 32'sh1ffff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_37 = {{32{z_12[31]}},z_12}; // @[Trig3.scala 44:22]
  wire [63:0] _z_13_T_3 = $signed(_GEN_37) - $signed(_z_13_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_14_T = $signed(d_14) * $signed(y_13); // @[Trig3.scala 42:29]
  wire [50:0] _x_14_T_1 = _x_14_T[63:13]; // @[Trig3.scala 42:37]
  wire [50:0] _GEN_38 = {{19{x_13[31]}},x_13}; // @[Trig3.scala 42:22]
  wire [50:0] _x_14_T_4 = $signed(_GEN_38) - $signed(_x_14_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_14_T = $signed(d_14) * $signed(x_13); // @[Trig3.scala 43:29]
  wire [50:0] _y_14_T_1 = _y_14_T[63:13]; // @[Trig3.scala 43:37]
  wire [50:0] _GEN_39 = {{19{y_13[31]}},y_13}; // @[Trig3.scala 43:22]
  wire [50:0] _y_14_T_4 = $signed(_GEN_39) + $signed(_y_14_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_14_T = $signed(d_14) * 32'shffff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_40 = {{32{z_13[31]}},z_13}; // @[Trig3.scala 44:22]
  wire [63:0] _z_14_T_3 = $signed(_GEN_40) - $signed(_z_14_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_15_T = $signed(d_15) * $signed(y_14); // @[Trig3.scala 42:29]
  wire [49:0] _x_15_T_1 = _x_15_T[63:14]; // @[Trig3.scala 42:37]
  wire [49:0] _GEN_41 = {{18{x_14[31]}},x_14}; // @[Trig3.scala 42:22]
  wire [49:0] _x_15_T_4 = $signed(_GEN_41) - $signed(_x_15_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_15_T = $signed(d_15) * $signed(x_14); // @[Trig3.scala 43:29]
  wire [49:0] _y_15_T_1 = _y_15_T[63:14]; // @[Trig3.scala 43:37]
  wire [49:0] _GEN_42 = {{18{y_14[31]}},y_14}; // @[Trig3.scala 43:22]
  wire [49:0] _y_15_T_4 = $signed(_GEN_42) + $signed(_y_15_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_15_T = $signed(d_15) * 32'sh7fff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_43 = {{32{z_14[31]}},z_14}; // @[Trig3.scala 44:22]
  wire [63:0] _z_15_T_3 = $signed(_GEN_43) - $signed(_z_15_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_16_T = $signed(d_16) * $signed(y_15); // @[Trig3.scala 42:29]
  wire [48:0] _x_16_T_1 = _x_16_T[63:15]; // @[Trig3.scala 42:37]
  wire [48:0] _GEN_44 = {{17{x_15[31]}},x_15}; // @[Trig3.scala 42:22]
  wire [48:0] _x_16_T_4 = $signed(_GEN_44) - $signed(_x_16_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_16_T = $signed(d_16) * $signed(x_15); // @[Trig3.scala 43:29]
  wire [48:0] _y_16_T_1 = _y_16_T[63:15]; // @[Trig3.scala 43:37]
  wire [48:0] _GEN_45 = {{17{y_15[31]}},y_15}; // @[Trig3.scala 43:22]
  wire [48:0] _y_16_T_4 = $signed(_GEN_45) + $signed(_y_16_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_16_T = $signed(d_16) * 32'sh3fff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_46 = {{32{z_15[31]}},z_15}; // @[Trig3.scala 44:22]
  wire [63:0] _z_16_T_3 = $signed(_GEN_46) - $signed(_z_16_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_17_T = $signed(d_17) * $signed(y_16); // @[Trig3.scala 42:29]
  wire [47:0] _x_17_T_1 = _x_17_T[63:16]; // @[Trig3.scala 42:37]
  wire [47:0] _GEN_47 = {{16{x_16[31]}},x_16}; // @[Trig3.scala 42:22]
  wire [47:0] _x_17_T_4 = $signed(_GEN_47) - $signed(_x_17_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_17_T = $signed(d_17) * $signed(x_16); // @[Trig3.scala 43:29]
  wire [47:0] _y_17_T_1 = _y_17_T[63:16]; // @[Trig3.scala 43:37]
  wire [47:0] _GEN_48 = {{16{y_16[31]}},y_16}; // @[Trig3.scala 43:22]
  wire [47:0] _y_17_T_4 = $signed(_GEN_48) + $signed(_y_17_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_17_T = $signed(d_17) * 32'sh1fff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_49 = {{32{z_16[31]}},z_16}; // @[Trig3.scala 44:22]
  wire [63:0] _z_17_T_3 = $signed(_GEN_49) - $signed(_z_17_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_18_T = $signed(d_18) * $signed(y_17); // @[Trig3.scala 42:29]
  wire [46:0] _x_18_T_1 = _x_18_T[63:17]; // @[Trig3.scala 42:37]
  wire [46:0] _GEN_50 = {{15{x_17[31]}},x_17}; // @[Trig3.scala 42:22]
  wire [46:0] _x_18_T_4 = $signed(_GEN_50) - $signed(_x_18_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_18_T = $signed(d_18) * $signed(x_17); // @[Trig3.scala 43:29]
  wire [46:0] _y_18_T_1 = _y_18_T[63:17]; // @[Trig3.scala 43:37]
  wire [46:0] _GEN_51 = {{15{y_17[31]}},y_17}; // @[Trig3.scala 43:22]
  wire [46:0] _y_18_T_4 = $signed(_GEN_51) + $signed(_y_18_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_18_T = $signed(d_18) * 32'shfff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_52 = {{32{z_17[31]}},z_17}; // @[Trig3.scala 44:22]
  wire [63:0] _z_18_T_3 = $signed(_GEN_52) - $signed(_z_18_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_19_T = $signed(d_19) * $signed(y_18); // @[Trig3.scala 42:29]
  wire [45:0] _x_19_T_1 = _x_19_T[63:18]; // @[Trig3.scala 42:37]
  wire [45:0] _GEN_53 = {{14{x_18[31]}},x_18}; // @[Trig3.scala 42:22]
  wire [45:0] _x_19_T_4 = $signed(_GEN_53) - $signed(_x_19_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_19_T = $signed(d_19) * $signed(x_18); // @[Trig3.scala 43:29]
  wire [45:0] _y_19_T_1 = _y_19_T[63:18]; // @[Trig3.scala 43:37]
  wire [45:0] _GEN_54 = {{14{y_18[31]}},y_18}; // @[Trig3.scala 43:22]
  wire [45:0] _y_19_T_4 = $signed(_GEN_54) + $signed(_y_19_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_19_T = $signed(d_19) * 32'sh7ff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_55 = {{32{z_18[31]}},z_18}; // @[Trig3.scala 44:22]
  wire [63:0] _z_19_T_3 = $signed(_GEN_55) - $signed(_z_19_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_20_T = $signed(d_20) * $signed(y_19); // @[Trig3.scala 42:29]
  wire [44:0] _x_20_T_1 = _x_20_T[63:19]; // @[Trig3.scala 42:37]
  wire [44:0] _GEN_56 = {{13{x_19[31]}},x_19}; // @[Trig3.scala 42:22]
  wire [44:0] _x_20_T_4 = $signed(_GEN_56) - $signed(_x_20_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_20_T = $signed(d_20) * $signed(x_19); // @[Trig3.scala 43:29]
  wire [44:0] _y_20_T_1 = _y_20_T[63:19]; // @[Trig3.scala 43:37]
  wire [44:0] _GEN_57 = {{13{y_19[31]}},y_19}; // @[Trig3.scala 43:22]
  wire [44:0] _y_20_T_4 = $signed(_GEN_57) + $signed(_y_20_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_20_T = $signed(d_20) * 32'sh3ff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_58 = {{32{z_19[31]}},z_19}; // @[Trig3.scala 44:22]
  wire [63:0] _z_20_T_3 = $signed(_GEN_58) - $signed(_z_20_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_21_T = $signed(d_21) * $signed(y_20); // @[Trig3.scala 42:29]
  wire [43:0] _x_21_T_1 = _x_21_T[63:20]; // @[Trig3.scala 42:37]
  wire [43:0] _GEN_59 = {{12{x_20[31]}},x_20}; // @[Trig3.scala 42:22]
  wire [43:0] _x_21_T_4 = $signed(_GEN_59) - $signed(_x_21_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_21_T = $signed(d_21) * $signed(x_20); // @[Trig3.scala 43:29]
  wire [43:0] _y_21_T_1 = _y_21_T[63:20]; // @[Trig3.scala 43:37]
  wire [43:0] _GEN_60 = {{12{y_20[31]}},y_20}; // @[Trig3.scala 43:22]
  wire [43:0] _y_21_T_4 = $signed(_GEN_60) + $signed(_y_21_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_21_T = $signed(d_21) * 32'sh1ff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_61 = {{32{z_20[31]}},z_20}; // @[Trig3.scala 44:22]
  wire [63:0] _z_21_T_3 = $signed(_GEN_61) - $signed(_z_21_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_22_T = $signed(d_22) * $signed(y_21); // @[Trig3.scala 42:29]
  wire [42:0] _x_22_T_1 = _x_22_T[63:21]; // @[Trig3.scala 42:37]
  wire [42:0] _GEN_62 = {{11{x_21[31]}},x_21}; // @[Trig3.scala 42:22]
  wire [42:0] _x_22_T_4 = $signed(_GEN_62) - $signed(_x_22_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_22_T = $signed(d_22) * $signed(x_21); // @[Trig3.scala 43:29]
  wire [42:0] _y_22_T_1 = _y_22_T[63:21]; // @[Trig3.scala 43:37]
  wire [42:0] _GEN_63 = {{11{y_21[31]}},y_21}; // @[Trig3.scala 43:22]
  wire [42:0] _y_22_T_4 = $signed(_GEN_63) + $signed(_y_22_T_1); // @[Trig3.scala 43:22]
  wire [63:0] _z_22_T = $signed(d_22) * 32'shff; // @[Trig3.scala 44:28]
  wire [63:0] _GEN_64 = {{32{z_21[31]}},z_21}; // @[Trig3.scala 44:22]
  wire [63:0] _z_22_T_3 = $signed(_GEN_64) - $signed(_z_22_T); // @[Trig3.scala 44:22]
  wire [63:0] _x_23_T = $signed(d_23) * $signed(y_22); // @[Trig3.scala 42:29]
  wire [41:0] _x_23_T_1 = _x_23_T[63:22]; // @[Trig3.scala 42:37]
  wire [41:0] _GEN_65 = {{10{x_22[31]}},x_22}; // @[Trig3.scala 42:22]
  wire [41:0] _x_23_T_4 = $signed(_GEN_65) - $signed(_x_23_T_1); // @[Trig3.scala 42:22]
  wire [63:0] _y_23_T = $signed(d_23) * $signed(x_22); // @[Trig3.scala 43:29]
  wire [41:0] _y_23_T_1 = _y_23_T[63:22]; // @[Trig3.scala 43:37]
  wire [41:0] _GEN_66 = {{10{y_22[31]}},y_22}; // @[Trig3.scala 43:22]
  wire [41:0] _y_23_T_4 = $signed(_GEN_66) + $signed(_y_23_T_1); // @[Trig3.scala 43:22]
  wire [32:0] _io_resultSin_T = {1'b0,$signed(32'h2431f1c7)}; // @[Trig3.scala 48:27]
  wire [64:0] _io_resultSin_T_1 = $signed(y_23) * $signed(_io_resultSin_T); // @[Trig3.scala 48:27]
  wire [63:0] _io_resultSin_T_3 = _io_resultSin_T_1[63:0]; // @[Trig3.scala 48:27]
  wire [64:0] _io_resultSin_T_4 = $signed(_io_resultSin_T_3) / 32'sh3b9aca00; // @[Trig3.scala 48:31]
  wire [64:0] _io_resultCos_T_1 = $signed(x_23) * $signed(_io_resultSin_T); // @[Trig3.scala 49:27]
  wire [63:0] _io_resultCos_T_3 = _io_resultCos_T_1[63:0]; // @[Trig3.scala 49:27]
  wire [64:0] _io_resultCos_T_4 = $signed(_io_resultCos_T_3) / 32'sh3b9aca00; // @[Trig3.scala 49:31]
  wire [63:0] _GEN_70 = reset ? $signed(64'sh0) : $signed(_x_1_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [62:0] _GEN_72 = reset ? $signed(63'sh0) : $signed(_x_2_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [61:0] _GEN_74 = reset ? $signed(62'sh0) : $signed(_x_3_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [60:0] _GEN_76 = reset ? $signed(61'sh0) : $signed(_x_4_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [59:0] _GEN_78 = reset ? $signed(60'sh0) : $signed(_x_5_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [58:0] _GEN_80 = reset ? $signed(59'sh0) : $signed(_x_6_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [57:0] _GEN_82 = reset ? $signed(58'sh0) : $signed(_x_7_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [56:0] _GEN_84 = reset ? $signed(57'sh0) : $signed(_x_8_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [55:0] _GEN_86 = reset ? $signed(56'sh0) : $signed(_x_9_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [54:0] _GEN_88 = reset ? $signed(55'sh0) : $signed(_x_10_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [53:0] _GEN_90 = reset ? $signed(54'sh0) : $signed(_x_11_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [52:0] _GEN_92 = reset ? $signed(53'sh0) : $signed(_x_12_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [51:0] _GEN_94 = reset ? $signed(52'sh0) : $signed(_x_13_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [50:0] _GEN_96 = reset ? $signed(51'sh0) : $signed(_x_14_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [49:0] _GEN_98 = reset ? $signed(50'sh0) : $signed(_x_15_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [48:0] _GEN_100 = reset ? $signed(49'sh0) : $signed(_x_16_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [47:0] _GEN_102 = reset ? $signed(48'sh0) : $signed(_x_17_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [46:0] _GEN_104 = reset ? $signed(47'sh0) : $signed(_x_18_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [45:0] _GEN_106 = reset ? $signed(46'sh0) : $signed(_x_19_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [44:0] _GEN_108 = reset ? $signed(45'sh0) : $signed(_x_20_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [43:0] _GEN_110 = reset ? $signed(44'sh0) : $signed(_x_21_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [42:0] _GEN_112 = reset ? $signed(43'sh0) : $signed(_x_22_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [41:0] _GEN_114 = reset ? $signed(42'sh0) : $signed(_x_23_T_4); // @[Trig3.scala 15:{18,18} 42:12]
  wire [63:0] _GEN_116 = reset ? $signed(64'sh0) : $signed(_y_1_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [62:0] _GEN_118 = reset ? $signed(63'sh0) : $signed(_y_2_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [61:0] _GEN_120 = reset ? $signed(62'sh0) : $signed(_y_3_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [60:0] _GEN_122 = reset ? $signed(61'sh0) : $signed(_y_4_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [59:0] _GEN_124 = reset ? $signed(60'sh0) : $signed(_y_5_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [58:0] _GEN_126 = reset ? $signed(59'sh0) : $signed(_y_6_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [57:0] _GEN_128 = reset ? $signed(58'sh0) : $signed(_y_7_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [56:0] _GEN_130 = reset ? $signed(57'sh0) : $signed(_y_8_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [55:0] _GEN_132 = reset ? $signed(56'sh0) : $signed(_y_9_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [54:0] _GEN_134 = reset ? $signed(55'sh0) : $signed(_y_10_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [53:0] _GEN_136 = reset ? $signed(54'sh0) : $signed(_y_11_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [52:0] _GEN_138 = reset ? $signed(53'sh0) : $signed(_y_12_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [51:0] _GEN_140 = reset ? $signed(52'sh0) : $signed(_y_13_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [50:0] _GEN_142 = reset ? $signed(51'sh0) : $signed(_y_14_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [49:0] _GEN_144 = reset ? $signed(50'sh0) : $signed(_y_15_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [48:0] _GEN_146 = reset ? $signed(49'sh0) : $signed(_y_16_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [47:0] _GEN_148 = reset ? $signed(48'sh0) : $signed(_y_17_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [46:0] _GEN_150 = reset ? $signed(47'sh0) : $signed(_y_18_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [45:0] _GEN_152 = reset ? $signed(46'sh0) : $signed(_y_19_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [44:0] _GEN_154 = reset ? $signed(45'sh0) : $signed(_y_20_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [43:0] _GEN_156 = reset ? $signed(44'sh0) : $signed(_y_21_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [42:0] _GEN_158 = reset ? $signed(43'sh0) : $signed(_y_22_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [41:0] _GEN_160 = reset ? $signed(42'sh0) : $signed(_y_23_T_4); // @[Trig3.scala 16:{18,18} 43:12]
  wire [63:0] _GEN_162 = reset ? $signed(64'sh0) : $signed(_z_1_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_164 = reset ? $signed(64'sh0) : $signed(_z_2_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_166 = reset ? $signed(64'sh0) : $signed(_z_3_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_168 = reset ? $signed(64'sh0) : $signed(_z_4_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_170 = reset ? $signed(64'sh0) : $signed(_z_5_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_172 = reset ? $signed(64'sh0) : $signed(_z_6_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_174 = reset ? $signed(64'sh0) : $signed(_z_7_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_176 = reset ? $signed(64'sh0) : $signed(_z_8_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_178 = reset ? $signed(64'sh0) : $signed(_z_9_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_180 = reset ? $signed(64'sh0) : $signed(_z_10_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_182 = reset ? $signed(64'sh0) : $signed(_z_11_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_184 = reset ? $signed(64'sh0) : $signed(_z_12_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_186 = reset ? $signed(64'sh0) : $signed(_z_13_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_188 = reset ? $signed(64'sh0) : $signed(_z_14_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_190 = reset ? $signed(64'sh0) : $signed(_z_15_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_192 = reset ? $signed(64'sh0) : $signed(_z_16_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_194 = reset ? $signed(64'sh0) : $signed(_z_17_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_196 = reset ? $signed(64'sh0) : $signed(_z_18_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_198 = reset ? $signed(64'sh0) : $signed(_z_19_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_200 = reset ? $signed(64'sh0) : $signed(_z_20_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_202 = reset ? $signed(64'sh0) : $signed(_z_21_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  wire [63:0] _GEN_204 = reset ? $signed(64'sh0) : $signed(_z_22_T_3); // @[Trig3.scala 17:{18,18} 44:12]
  assign io_resultSin = _io_resultSin_T_4[31:0]; // @[Trig3.scala 48:16]
  assign io_resultCos = _io_resultCos_T_4[31:0]; // @[Trig3.scala 49:16]
  always @(posedge clock) begin
    if (reset) begin // @[Trig3.scala 15:18]
      x_0 <= 32'sh0; // @[Trig3.scala 15:18]
    end else begin
      x_0 <= 32'sh20000000; // @[Trig3.scala 36:12]
    end
    x_1 <= _GEN_70[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_2 <= _GEN_72[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_3 <= _GEN_74[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_4 <= _GEN_76[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_5 <= _GEN_78[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_6 <= _GEN_80[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_7 <= _GEN_82[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_8 <= _GEN_84[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_9 <= _GEN_86[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_10 <= _GEN_88[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_11 <= _GEN_90[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_12 <= _GEN_92[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_13 <= _GEN_94[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_14 <= _GEN_96[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_15 <= _GEN_98[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_16 <= _GEN_100[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_17 <= _GEN_102[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_18 <= _GEN_104[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_19 <= _GEN_106[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_20 <= _GEN_108[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_21 <= _GEN_110[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_22 <= _GEN_112[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    x_23 <= _GEN_114[31:0]; // @[Trig3.scala 15:{18,18} 42:12]
    y_1 <= _GEN_116[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_2 <= _GEN_118[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_3 <= _GEN_120[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_4 <= _GEN_122[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_5 <= _GEN_124[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_6 <= _GEN_126[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_7 <= _GEN_128[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_8 <= _GEN_130[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_9 <= _GEN_132[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_10 <= _GEN_134[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_11 <= _GEN_136[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_12 <= _GEN_138[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_13 <= _GEN_140[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_14 <= _GEN_142[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_15 <= _GEN_144[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_16 <= _GEN_146[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_17 <= _GEN_148[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_18 <= _GEN_150[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_19 <= _GEN_152[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_20 <= _GEN_154[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_21 <= _GEN_156[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_22 <= _GEN_158[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    y_23 <= _GEN_160[31:0]; // @[Trig3.scala 16:{18,18} 43:12]
    if (reset) begin // @[Trig3.scala 17:18]
      z_0 <= 32'sh0; // @[Trig3.scala 17:18]
    end else begin
      z_0 <= io_input; // @[Trig3.scala 38:12]
    end
    z_1 <= _GEN_162[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_2 <= _GEN_164[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_3 <= _GEN_166[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_4 <= _GEN_168[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_5 <= _GEN_170[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_6 <= _GEN_172[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_7 <= _GEN_174[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_8 <= _GEN_176[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_9 <= _GEN_178[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_10 <= _GEN_180[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_11 <= _GEN_182[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_12 <= _GEN_184[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_13 <= _GEN_186[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_14 <= _GEN_188[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_15 <= _GEN_190[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_16 <= _GEN_192[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_17 <= _GEN_194[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_18 <= _GEN_196[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_19 <= _GEN_198[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_20 <= _GEN_200[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_21 <= _GEN_202[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    z_22 <= _GEN_204[31:0]; // @[Trig3.scala 17:{18,18} 44:12]
    if (reset) begin // @[Trig3.scala 18:18]
      d_1 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_0) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_1 <= -32'sh1;
    end else begin
      d_1 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_2 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_1) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_2 <= -32'sh1;
    end else begin
      d_2 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_3 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_2) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_3 <= -32'sh1;
    end else begin
      d_3 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_4 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_3) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_4 <= -32'sh1;
    end else begin
      d_4 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_5 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_4) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_5 <= -32'sh1;
    end else begin
      d_5 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_6 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_5) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_6 <= -32'sh1;
    end else begin
      d_6 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_7 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_6) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_7 <= -32'sh1;
    end else begin
      d_7 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_8 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_7) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_8 <= -32'sh1;
    end else begin
      d_8 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_9 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_8) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_9 <= -32'sh1;
    end else begin
      d_9 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_10 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_9) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_10 <= -32'sh1;
    end else begin
      d_10 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_11 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_10) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_11 <= -32'sh1;
    end else begin
      d_11 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_12 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_11) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_12 <= -32'sh1;
    end else begin
      d_12 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_13 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_12) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_13 <= -32'sh1;
    end else begin
      d_13 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_14 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_13) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_14 <= -32'sh1;
    end else begin
      d_14 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_15 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_14) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_15 <= -32'sh1;
    end else begin
      d_15 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_16 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_15) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_16 <= -32'sh1;
    end else begin
      d_16 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_17 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_16) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_17 <= -32'sh1;
    end else begin
      d_17 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_18 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_17) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_18 <= -32'sh1;
    end else begin
      d_18 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_19 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_18) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_19 <= -32'sh1;
    end else begin
      d_19 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_20 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_19) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_20 <= -32'sh1;
    end else begin
      d_20 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_21 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_20) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_21 <= -32'sh1;
    end else begin
      d_21 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_22 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_21) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_22 <= -32'sh1;
    end else begin
      d_22 <= 32'sh1;
    end
    if (reset) begin // @[Trig3.scala 18:18]
      d_23 <= 32'sh0; // @[Trig3.scala 18:18]
    end else if ($signed(z_22) < 32'sh0) begin // @[Trig3.scala 41:18]
      d_23 <= -32'sh1;
    end else begin
      d_23 <= 32'sh1;
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  x_0 = _RAND_0[31:0];
  _RAND_1 = {1{`RANDOM}};
  x_1 = _RAND_1[31:0];
  _RAND_2 = {1{`RANDOM}};
  x_2 = _RAND_2[31:0];
  _RAND_3 = {1{`RANDOM}};
  x_3 = _RAND_3[31:0];
  _RAND_4 = {1{`RANDOM}};
  x_4 = _RAND_4[31:0];
  _RAND_5 = {1{`RANDOM}};
  x_5 = _RAND_5[31:0];
  _RAND_6 = {1{`RANDOM}};
  x_6 = _RAND_6[31:0];
  _RAND_7 = {1{`RANDOM}};
  x_7 = _RAND_7[31:0];
  _RAND_8 = {1{`RANDOM}};
  x_8 = _RAND_8[31:0];
  _RAND_9 = {1{`RANDOM}};
  x_9 = _RAND_9[31:0];
  _RAND_10 = {1{`RANDOM}};
  x_10 = _RAND_10[31:0];
  _RAND_11 = {1{`RANDOM}};
  x_11 = _RAND_11[31:0];
  _RAND_12 = {1{`RANDOM}};
  x_12 = _RAND_12[31:0];
  _RAND_13 = {1{`RANDOM}};
  x_13 = _RAND_13[31:0];
  _RAND_14 = {1{`RANDOM}};
  x_14 = _RAND_14[31:0];
  _RAND_15 = {1{`RANDOM}};
  x_15 = _RAND_15[31:0];
  _RAND_16 = {1{`RANDOM}};
  x_16 = _RAND_16[31:0];
  _RAND_17 = {1{`RANDOM}};
  x_17 = _RAND_17[31:0];
  _RAND_18 = {1{`RANDOM}};
  x_18 = _RAND_18[31:0];
  _RAND_19 = {1{`RANDOM}};
  x_19 = _RAND_19[31:0];
  _RAND_20 = {1{`RANDOM}};
  x_20 = _RAND_20[31:0];
  _RAND_21 = {1{`RANDOM}};
  x_21 = _RAND_21[31:0];
  _RAND_22 = {1{`RANDOM}};
  x_22 = _RAND_22[31:0];
  _RAND_23 = {1{`RANDOM}};
  x_23 = _RAND_23[31:0];
  _RAND_24 = {1{`RANDOM}};
  y_1 = _RAND_24[31:0];
  _RAND_25 = {1{`RANDOM}};
  y_2 = _RAND_25[31:0];
  _RAND_26 = {1{`RANDOM}};
  y_3 = _RAND_26[31:0];
  _RAND_27 = {1{`RANDOM}};
  y_4 = _RAND_27[31:0];
  _RAND_28 = {1{`RANDOM}};
  y_5 = _RAND_28[31:0];
  _RAND_29 = {1{`RANDOM}};
  y_6 = _RAND_29[31:0];
  _RAND_30 = {1{`RANDOM}};
  y_7 = _RAND_30[31:0];
  _RAND_31 = {1{`RANDOM}};
  y_8 = _RAND_31[31:0];
  _RAND_32 = {1{`RANDOM}};
  y_9 = _RAND_32[31:0];
  _RAND_33 = {1{`RANDOM}};
  y_10 = _RAND_33[31:0];
  _RAND_34 = {1{`RANDOM}};
  y_11 = _RAND_34[31:0];
  _RAND_35 = {1{`RANDOM}};
  y_12 = _RAND_35[31:0];
  _RAND_36 = {1{`RANDOM}};
  y_13 = _RAND_36[31:0];
  _RAND_37 = {1{`RANDOM}};
  y_14 = _RAND_37[31:0];
  _RAND_38 = {1{`RANDOM}};
  y_15 = _RAND_38[31:0];
  _RAND_39 = {1{`RANDOM}};
  y_16 = _RAND_39[31:0];
  _RAND_40 = {1{`RANDOM}};
  y_17 = _RAND_40[31:0];
  _RAND_41 = {1{`RANDOM}};
  y_18 = _RAND_41[31:0];
  _RAND_42 = {1{`RANDOM}};
  y_19 = _RAND_42[31:0];
  _RAND_43 = {1{`RANDOM}};
  y_20 = _RAND_43[31:0];
  _RAND_44 = {1{`RANDOM}};
  y_21 = _RAND_44[31:0];
  _RAND_45 = {1{`RANDOM}};
  y_22 = _RAND_45[31:0];
  _RAND_46 = {1{`RANDOM}};
  y_23 = _RAND_46[31:0];
  _RAND_47 = {1{`RANDOM}};
  z_0 = _RAND_47[31:0];
  _RAND_48 = {1{`RANDOM}};
  z_1 = _RAND_48[31:0];
  _RAND_49 = {1{`RANDOM}};
  z_2 = _RAND_49[31:0];
  _RAND_50 = {1{`RANDOM}};
  z_3 = _RAND_50[31:0];
  _RAND_51 = {1{`RANDOM}};
  z_4 = _RAND_51[31:0];
  _RAND_52 = {1{`RANDOM}};
  z_5 = _RAND_52[31:0];
  _RAND_53 = {1{`RANDOM}};
  z_6 = _RAND_53[31:0];
  _RAND_54 = {1{`RANDOM}};
  z_7 = _RAND_54[31:0];
  _RAND_55 = {1{`RANDOM}};
  z_8 = _RAND_55[31:0];
  _RAND_56 = {1{`RANDOM}};
  z_9 = _RAND_56[31:0];
  _RAND_57 = {1{`RANDOM}};
  z_10 = _RAND_57[31:0];
  _RAND_58 = {1{`RANDOM}};
  z_11 = _RAND_58[31:0];
  _RAND_59 = {1{`RANDOM}};
  z_12 = _RAND_59[31:0];
  _RAND_60 = {1{`RANDOM}};
  z_13 = _RAND_60[31:0];
  _RAND_61 = {1{`RANDOM}};
  z_14 = _RAND_61[31:0];
  _RAND_62 = {1{`RANDOM}};
  z_15 = _RAND_62[31:0];
  _RAND_63 = {1{`RANDOM}};
  z_16 = _RAND_63[31:0];
  _RAND_64 = {1{`RANDOM}};
  z_17 = _RAND_64[31:0];
  _RAND_65 = {1{`RANDOM}};
  z_18 = _RAND_65[31:0];
  _RAND_66 = {1{`RANDOM}};
  z_19 = _RAND_66[31:0];
  _RAND_67 = {1{`RANDOM}};
  z_20 = _RAND_67[31:0];
  _RAND_68 = {1{`RANDOM}};
  z_21 = _RAND_68[31:0];
  _RAND_69 = {1{`RANDOM}};
  z_22 = _RAND_69[31:0];
  _RAND_70 = {1{`RANDOM}};
  d_1 = _RAND_70[31:0];
  _RAND_71 = {1{`RANDOM}};
  d_2 = _RAND_71[31:0];
  _RAND_72 = {1{`RANDOM}};
  d_3 = _RAND_72[31:0];
  _RAND_73 = {1{`RANDOM}};
  d_4 = _RAND_73[31:0];
  _RAND_74 = {1{`RANDOM}};
  d_5 = _RAND_74[31:0];
  _RAND_75 = {1{`RANDOM}};
  d_6 = _RAND_75[31:0];
  _RAND_76 = {1{`RANDOM}};
  d_7 = _RAND_76[31:0];
  _RAND_77 = {1{`RANDOM}};
  d_8 = _RAND_77[31:0];
  _RAND_78 = {1{`RANDOM}};
  d_9 = _RAND_78[31:0];
  _RAND_79 = {1{`RANDOM}};
  d_10 = _RAND_79[31:0];
  _RAND_80 = {1{`RANDOM}};
  d_11 = _RAND_80[31:0];
  _RAND_81 = {1{`RANDOM}};
  d_12 = _RAND_81[31:0];
  _RAND_82 = {1{`RANDOM}};
  d_13 = _RAND_82[31:0];
  _RAND_83 = {1{`RANDOM}};
  d_14 = _RAND_83[31:0];
  _RAND_84 = {1{`RANDOM}};
  d_15 = _RAND_84[31:0];
  _RAND_85 = {1{`RANDOM}};
  d_16 = _RAND_85[31:0];
  _RAND_86 = {1{`RANDOM}};
  d_17 = _RAND_86[31:0];
  _RAND_87 = {1{`RANDOM}};
  d_18 = _RAND_87[31:0];
  _RAND_88 = {1{`RANDOM}};
  d_19 = _RAND_88[31:0];
  _RAND_89 = {1{`RANDOM}};
  d_20 = _RAND_89[31:0];
  _RAND_90 = {1{`RANDOM}};
  d_21 = _RAND_90[31:0];
  _RAND_91 = {1{`RANDOM}};
  d_22 = _RAND_91[31:0];
  _RAND_92 = {1{`RANDOM}};
  d_23 = _RAND_92[31:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule

